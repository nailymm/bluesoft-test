# BluebankFE

Este proyecto fue generado en [Angular CLI](https://github.com/angular/angular-cli) version 12.1.1.

## Especificaciones tecnicas
Angular 12.1.1

## Setup:
Clonar repositorio.
`$ git clone https://gitlab.com/nailymm/bluesoft-test.git`
`$ cd bluesoft-test`

Hacer el install con npm.
`$ npm install`

Ejecutar con 
`$ ng serve`

La aplicacion queda disponible en: http://localhost:4200

## Mejoras futuras:
- Aplicacion de Angular Material o cualquier plantilla customizable.
- Implementación de Login
- Uso de herramientas de auditoría
- Implementación de mensajería