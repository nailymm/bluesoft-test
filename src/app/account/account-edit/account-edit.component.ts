import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-account-edit',
  templateUrl: './account-edit.component.html',
  styleUrls: ['./account-edit.component.scss']
})
export class AccountEditComponent implements OnInit {
  account = {
    name: '',
    balance: 0.0,
    accountNumber: 0,
    clientName: ''
  };
  submitted = false;

  constructor(private accountService: AccountService) { }

  ngOnInit(): void {
  }
  createAccount(): void {
    const data = {
      name: this.account.name,
      balance: this.account.balance,
      accountNumber: this.account.accountNumber,
      clientName: this.account.clientName
    };

    this.accountService.create$(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newAccount(): void {
    this.submitted = false;
    this.account = {
      name: '',
      balance: 0.0,
      accountNumber: 0,
      clientName: ''
    };
  }
}
