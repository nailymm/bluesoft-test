import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {

  accounts: any;
  currentAccount: any | undefined ;
  currentIndex = -1;
  name = '';

  constructor(private accountService: AccountService) { }

  ngOnInit(): void {
    this.readAccounts();
  }

  readAccounts(): void {
    this.accountService.readAll$()
      .subscribe(
        accounts => {
          this.accounts = accounts;
          console.log(accounts);
        },
        error => {
          console.log(error);
        });
  }

  refresh(): void {
    this.readAccounts();
    this.currentAccount = undefined;
    this.currentIndex = -1;
  }

  setCurrentAccount(account: any, index: number): void {
    this.currentAccount = account;
    this.currentIndex = index;
  }

}
