import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountDetailComponent } from './account/account-detail/account-detail.component';
import { AccountEditComponent } from './account/account-edit/account-edit.component';
import { AccountListComponent } from './account/account-list/account-list.component';
import { AccountComponent } from './account/account.component';
import { TransactionCreateComponent } from './transaction/transaction-create/transaction-create.component';
import { TransactionListComponent } from './transaction/transaction-list/transaction-list.component';

const routes: Routes = [
  { path: 'accounts', component: AccountListComponent },
  { path: 'accounts/:id', component: AccountDetailComponent },
  { path: 'accounts/:id/create', component: AccountEditComponent },
  { path: 'transactions', component: TransactionListComponent },
  { path: 'transactions/:id/create', component:  TransactionCreateComponent},
  { path: '**', redirectTo: 'not-found' },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}