import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountComponent } from './account/account.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountListComponent } from './account/account-list/account-list.component';
import { AccountDetailComponent } from './account/account-detail/account-detail.component';
import { AccountEditComponent } from './account/account-edit/account-edit.component';
import { TransactionListComponent } from './transaction/transaction-list/transaction-list.component';
import { TransactionCreateComponent } from './transaction/transaction-create/transaction-create.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    AccountListComponent,
    AccountDetailComponent,
    AccountEditComponent,
    TransactionListComponent,
    TransactionCreateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
