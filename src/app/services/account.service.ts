import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const baseURL = 'http://localhost:8080/api/account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private httpClient: HttpClient) { }

  readAll$(): Observable<any> {
    return this.httpClient.get(baseURL+'/allAccounts');
  }

  read$(id: number): Observable<any> {
    return this.httpClient.get(`${baseURL}/${id}`);
  }

  create$(data: Object): Observable<any> {
    return this.httpClient.post(baseURL + '/insert', data);
  }

  update$(id: number, data: Object): Observable<any> {
    return this.httpClient.put(`${baseURL}/insert/${id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.httpClient.delete(`${baseURL}/${id}/delete`);
  }


}
