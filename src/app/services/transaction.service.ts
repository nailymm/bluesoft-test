import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const baseURL = 'http://localhost:8080/api/transaction';
@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private httpClient: HttpClient) { }

  readAll$(): Observable<any> {
    return this.httpClient.get(baseURL+'/all');
  }
  create$(data: Object): Observable<any> {
    return this.httpClient.post(baseURL + '/insert', data);
  }
  withdraw$(data: Object): Observable<any> {
    return this.httpClient.post(baseURL + '/withdraw', data);
  }
}
