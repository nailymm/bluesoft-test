import { Component, OnInit } from '@angular/core';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-transaction-create',
  templateUrl: './transaction-create.component.html',
  styleUrls: ['./transaction-create.component.scss']
})
export class TransactionCreateComponent implements OnInit {
  transaction = {
    source: '',
    target: '',
    amount: 0.0,
    type: '',
    status: ''
  };
  typesTransaction = [ {id: 'DEPOSIT', name: 'Depósito'}, 
           {id: 'WITHDRAW', name: 'Retiro'}, 
           {id: 'TRANSFERENCE', name: 'Transferencia'}];
  submitted = false;
  selectedValue: any = null;

  constructor(private transactionService: TransactionService) { }

  ngOnInit(): void {
  }

  create(): void {
    const data = {
      source: this.transaction.source,
      target: (this.selectedValue == 'DEPOSIT' || this.selectedValue == 'WITHDRAW') ?  this.transaction.source : this.transaction.target,
      amount: this.transaction.amount,
      type: this.selectedValue,
      status: 'SUCCESS'
    };

    switch (this.selectedValue) {
      case 'TRANSFERENCE':
      case 'DEPOSIT':
        this.transactionService.create$(data)
        .subscribe(
          response => {
            console.log(response);
            this.submitted = true;
          },
          error => {
            console.log(error);
          });
        break;
      case 'WITHDRAW':
        this.transactionService.withdraw$(data)
        .subscribe(
          response => {
            console.log(response);
            this.submitted = true;
          },
          error => {
            console.log(error);
          });
        break;
    
      default:
        break;
    }

  }

  newTransaction(): void {
    this.submitted = false;
    this.selectedValue = null;
    this.transaction = {
      source: '',
      target: '',
      amount: 0.0,
      type: '',
      status: ''
    };
  }

}
