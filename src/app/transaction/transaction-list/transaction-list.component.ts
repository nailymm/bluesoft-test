import { Component, OnInit } from '@angular/core';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit {
  transactions: any;
  currentTransaction: any | undefined ;
  currentIndex = -1;
  name = '';

  constructor(private transactionService: TransactionService) { }

  ngOnInit(): void {
    this.readTransactions();
  }

  readTransactions(): void {
    this.transactionService.readAll$()
      .subscribe(
        transactions => {
          this.transactions = transactions;
          console.log(transactions);
        },
        error => {
          console.log(error);
        });
  }

  refresh(): void {
    this.readTransactions();
    this.currentTransaction = undefined;
    this.currentIndex = -1;
  }

  setCurrentTransaction(transaction: any, index: number): void {
    this.currentTransaction = transaction;
    this.currentIndex = index;
  }
}
